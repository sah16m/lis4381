package com.example.businesscard;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        android.app.ActionBar actionBar = getActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayOptions(actionBar.DISPLAY_SHOW_HOME | actionBar.DISPLAY_SHOW_TITLE);
        actionBar.setIcon(R.mipmap.olaf);

        Button button = (Button)  findViewById(R.id.bttnRecipe);
        button.setOnClickListener((v) ->
        {
            startActivity(new Intent(MainActivity.this, Recipe.class));
        });
    }
}