# LIS4381 - Mobile Web Application Development

## Sarah Huerta

### Project 2 Requirements:

*Parts*

1. Update previous Assignment 4 and 5 to continue C.R.U.D. protocol.
2. Project 2 works on Update and Deleting Records from database
3. Includes RSS Feed of creators chosing.


#### Assignment Screenshots:



### Landing Page and Screenshot of P2 Index.php:

| Landing Page|  Index.php | RSS Feed |
| -----------------------------------| ----------------------------------- | ----------------------------------- |
| ![Landing Page](img/hp.png) | ![Index.php](img/start.png) | ![RSS Feed](img/rss.png) |

### Edit Petstore

| Pre Update | Edit Row |
| ----------------------------------- | ----------------------------------- |
| ![Pre Update](img/preupdate.png) | ![Update Edit](img/update.png) |


|  Faild Validation | Passed Updated |
| ----------------------------------- | ----------------------------------- |
| ![Update Error](img/invalid.png) | ![Update Complete](img/complete.png) |


### Delete Petstore
|Pre Delete | Delete Record |  Sucessful Deletetion |
| -----------------------------------| ----------------------------------- | ----------------------------------- |
|  ![PreDelete](img/predelete.png) |![Mid Delete](img/middelete.png) | ![Post Delete](img/postdelete.png) |
